﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.Equipment
{
    public class BXJGEquipmentConst
    {
        public const string LocalizationSourceName = "BXJGEquipment";

        #region 设备信息
        public const int EquipmentInfoNameMaxLength = 200;
        public const int EquipmentInfoHardwareCodeMaxLength = 50;
        //public const int EquipmentInfoMnemonicCodeMaxLength = 200;
        //public const int EquipmentInfoSizeMaxLength = 200;
        #endregion
    }
}
