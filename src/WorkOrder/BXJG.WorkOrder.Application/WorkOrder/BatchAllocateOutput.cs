﻿using BXJG.Common.Dto;

namespace BXJG.WorkOrder.WorkOrder
{
    /// <summary>
    /// 后台管理批量分配工单的返回模型基类
    /// </summary>
    public class WorkOrderBatchAllocateOutputBase : BatchOperationOutputLong
    {
    }
}