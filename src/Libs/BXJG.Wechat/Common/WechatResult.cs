﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.WeChat.Common
{
    public class WechatResult
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
